# Overview
Analysis pipeline for Calcium traces, read the attached articles for more info.

# Folder structure

- the data file headers should look like:  TimeSample     Mean1	XM1	YM1	Mean2	XM2	YM2 .....
- The file name should match *_data.dat
- The info about the optics needed:
![metadata](metadata.png)

