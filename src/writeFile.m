function bol = writeFile(action)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%              Spectral Analysis of Calcium Oscillations              %
%                            by  Per Uhlen                            %
%                                                                     %
%                             Version 3.0                             %
%                                                                     %
%           Copyright 2004 by Per Uhlen All Rights Reserved           %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%  Program file: SpectralAnalysis.m                                   %
%  Required files: readFile.m; writeFile.m;                           %
%  1st version: 1999-12-29, Karolinska Institutet, Stockholm, Sweden  %
%  2nd version: 2004-04-29, Yale University, New Haven, CT, U.S.A.    %
%  3d  version: 2004-10-20, Yale University, New Haven, CT, U.S.A.    %
%                                                                     %
%  Contact: per.uhlen@yale.edu; per.uhlen@kbh.ki.se                   %
%                                                                     %
%  Last updated: 2004-10-26                                           %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global pow
  
% This file contains commands to write data to files.

if strcmpi(action,'header')
  
  % Write the header to the file pow.file.target
  [fid, message] = fopen(pow.file.target,'w');
  if ~isempty(message)
    fprintf('The file %s could not be opened\n', pow.file.target)
    bol = 0;
    return
  else
    fprintf(fid,'%% Spectrum analysis of file %s\n', pow.file.source);
    fprintf(fid,'%% The file contains %g cells\n',pow.noCells);
    fprintf(fid,'%% Sampling frequency is: %g (mHz)\n',pow.sampleFreq);
    fprintf(fid,'%% Nyquist frequency is: %g (mHz)\n',pow.nyquist);
    fprintf(fid,'%% The Fast Fourier Transform was used with N=%g\n',pow.N);
    fprintf(fid,'%% %s filter was used\n',pow.filter);
    fprintf(fid,'%% ---\n');
    if pow.areaSelect
      fprintf(fid,'%% Cell number, Frequency(mHz), Relative Power(%%), ...\n');
    else
      fprintf(fid,'%% Cell number, Frequency(mHz), Peak Height (unit), ...\n');
    end
    fprintf(fid,'%% ---\n');
    fclose(fid);
  end
  
  bol = 1;
  return
  
elseif strcmpi(action,'data')
  
  % Write the analyzed data to the file pow.file.target
  
  [fid, message] = fopen(pow.file.target,'a');
  if ~isempty(message)
    fprintf('The file %s could not be opened\n',pow.file.target)
    bol = 0;
    return
  else
    if ~strcmpi(pow.lastKey,'n')
      if pow.areaSelect
	peakType = 3;
      else
	peakType = 2;
      end
      
      fprintf(fid,'%d,',pow.counter); %Write the cell number
      
      for n=1:pow.noPeaks
	fprintf(fid,' %8.4g %7.4g',pow.peaks(n,1),pow.peaks(n,peakType));
      end
      fprintf(fid,'\n');
    else
      fprintf(fid,'   --.--   --.--\n');
    end
    fclose(fid);
  end
  
  bol = 1;
  return
  
elseif strcmpi(action,'footer')
  
  % Write the footer to the file pow.file.target
  
  [fid, message] = fopen(pow.file.target,'a');
  if ~isempty(message)
    fprintf('The file %s could not be opened\n',pow.file.target)
    bol = 0;
    return
  else
    fprintf(fid,'%% ---\n');
    fprintf(fid, '%% Mean frequency: %f mHz \n',pow.freq.meanValue);
    fprintf(fid, '%% SEM: %f mHz \n',pow.freq.semValue);
    fprintf(fid, '%% Standard deviation: %f mHz \n',pow.freq.StD);
    fprintf(fid, '%% Coefficient of variation: %f \n',pow.freq.CoV);
    fprintf(fid,'%%********************************************\n');
    fprintf(fid,'\n');
    fclose(fid);
  end
  
  bol = 1;
  return  
  
elseif strcmpi(action,'power')
  
  % Write the power spectrum to the file pow.file.power
  
  % Increment for the power spectrum to reduce the file size 
  % pow.increment is set in the main file
  
  % Create the name for the spectrum file
  index = max(strfind(pow.file.target,'.'));
  ext = pow.file.target(index:length(pow.file.target));
  ext = strcat('spectrum',ext);
  pow.file.power = strcat(pow.file.target(1:index),ext);
  
  [fid, message] = fopen(pow.file.power,'w');
  if ~isempty(message)
    fprintf('The file %s could not be opened\n',pow.file.power)
    bol = 0;
    return
  else
    fprintf(fid,'%%Power spectrum from File %s\n',pow.file.source);
    fprintf(fid,'%%Frequency in mHz and Power values\n');
    fprintf(fid,'%%Freq,  Cell 1,         Cell 2,         Cell 3,         ...\n');  
    
    [m,n] = size(pow.B);
    h = waitbar(0,'Please wait...');
    for i = 1:pow.increment:m
      waitbar(i/m)
      fprintf(fid,'%5.2f\t',pow.B(i,1));
      for j = 2:n
	fprintf(fid,'%.3e\t',pow.B(i,j));
      end
      fprintf(fid,'\n');
    end
    fclose(fid);
    close(h);
  end
  bol = 1;
  return
  
elseif strcmpi(action,'init')
  
  % Write the last used input values to file pow.file.init

  [fid, message] = fopen(pow.file.init,'w');
  if ~isempty(message)
    fprintf('The initialization data could not be stored\n')
    bol = 0;
    return
  else
    fprintf(fid,'%s\n',pow.dialog.answer{:});
    fclose(fid);
  end
  bol = 1;
  return
    
else
  
  bol = 0;
  return
  
end
