function bol = readFile(action)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%              Spectral Analysis of Calcium Oscillations              %
%                            by  Per Uhlen                            %
%                                                                     %
%                             Version 3.0                             %
%                                                                     %
%           Copyright 2004 by Per Uhlen All Rights Reserved           %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%  Program file: SpectralAnalysis.m                                   %
%  Required files: readFile.m; writeFile.m;                           %
%  1st version: 1999-12-29, Karolinska Institutet, Stockholm, Sweden  %
%  2nd version: 2004-04-29, Yale University, New Haven, CT, U.S.A.    %
%  3d  version: 2004-10-20, Yale University, New Haven, CT, U.S.A.    %
%                                                                     %
%  Contact: per.uhlen@yale.edu; per.uhlen@kbh.ki.se                   %
%                                                                     %
%  Last updated: 2004-10-26                                           %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global pow

% This file contains commands to read the original calcium recordings
% into a matrix that could be handled by Matlab. The original data
% should be arranged with the time values in the first column and each
% single cell recording in the following columns. Columns should be
% separated with tabs.

if strcmpi(action,'oscillations')
    
    % Read source data from the file pow.file.source
    
    % tCol is the number of the time column
    tCol = 1;
    
    [fid, message] = fopen(pow.file.source,'r');
    if ~isempty(message)
        fprintf('The file %s could not opend\n',pow.file.source)
        bol = 0;
        return
    else
        line = str2num(strrep(fgets(fid),',','.'));
        while isempty(line)
            line = str2num(strrep(fgets(fid),',','.'));
        end
        
        pow.A = line(tCol:length(line));
        while ~feof(fid)
            line = str2num(strrep(fgets(fid),',','.'));
            if ~isempty(line)
                pow.A = cat(1,pow.A,line(tCol:length(line)));
            end
        end
        fclose(fid);
    end
    
    bol = 1;
    return
    
elseif strcmpi(action,'init')
    
    % Read the last used input values from file pow.file.init
    % If the file does not exist default values are used
    
    [fid, message] = fopen(pow.file.init,'r');
    if ~isempty(message)
        pow.dialog.def = {'samples.txt','c:\target.dat','10', ...
            'auto','area','hanning','2'};
    else
        s = [];
        while 1
            initLine = fgetl(fid);
            if ~ischar(initLine), break, end
            s = strvcat(s,initLine);
        end
        pow.dialog.def = cellstr(s);
        fclose(fid);
    end
    
    bol = 1;
    return
    
else
    
    bol = 0;
    return
    
end

