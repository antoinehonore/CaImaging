function info = getinfo(File,info)
% function info = getinfo(File,info)
% fill the info structure with the path and filename of a single data file

    tmp=    regexp(File.name,'/');
    if isempty(tmp)
        tmp=regexp(File.name,'\');
    end
    if isempty(tmp)
        error('Error in file selection');
    end

    %The actual filename starts after the last folder delimiter
    info.filename=  File.name(tmp(end)+1:end);
    %
    info.filepath= File.folder;
end