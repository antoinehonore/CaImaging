function  data = readFile(filename)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    % Read source data from the file filename
    
    % tCol is the number of the time column
    tCol = 1;
    
    [fid, message] = fopen(filename,'r');
    if ~isempty(message)
        fprintf('The file %s could not opend\n',filename)
        bol = 0;
        return
    else
        line = str2num(strrep(fgets(fid),',','.'));
        while isempty(line)
            line = str2num(strrep(fgets(fid),',','.'));
        end
        
        data = line(tCol:length(line));
        while ~feof(fid)
            line = str2num(strrep(fgets(fid),',','.'));
            if ~isempty(line)
                data = cat(1,data,line(tCol:length(line)));
            end
        end
        fclose(fid);
    end
    
    bol = 1;
  
    
end

