%
%pickcells.m
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%                       Network Identification                        %
%                by  Erik Smedler MSc, Phd student                    %
%                                                                     %
%                        Version 3.0 Publication                      %
%                                                                     %
%           Copyright 2014 by Erik Smedler All Rights Reserved        %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%  Program file: NetworkIdentification.m                              %
%  Related needed files: crosscorrelation.m; pickcells.m;             %
%  Required file: oscillationdata.dat                                 %  
%  Creates file: correlationdata.mat                                  %
%  1st version: 2008-12-12, Karolinska Institutet, Stockholm, Sweden  %
%  2nd version: 2011-09-24, Karolinska Institutet, Stockholm, Sweden  %
%  3nd version: 2014-01-23, Karolinska Institutet, Stockholm, Sweden  %
%                                                                     %
%                                                                     %
%  Contact: erik.smedler@ki.se                                        %
%                                                                     %
%  Last updated: 2014-01-23                                           %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - - - Main file for the NetworkIdentification program - - - %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%
%--------------------------------------------------------------------------
%
%Pick active cells
%Program that allows the user to pick active cells according to some
%definition. 
%
%--------------------------------------------------------------------------
function indexofcells= pickcells(celldata,info,opts)
global picked_indices discardCells i  old_indexofcells picked_flag;
%Input values.


%Save the parameter choice.
y=opts.networkid;

% If the user does not press Cancel.
% if ~isempty(answer)
%     source = char('signaldata.txt');
%     targetExt       = char('.dat');
    highValue       = str2num(char(opts.networkid(end-1)));
    trendDegree     = str2num(char(opts.networkid(end)));
    startPoint      = 1;    
    autoNorm        = char('yes');
%     target = strcat(source,targetExt);  
%     
%     %Load data to pick from.
%     expdata=load(source);

%     expdata(1,:)=[];
    
%     celldata=expdata(:,2:end);
    numberofcells=size(celldata,2);
    old_indexofcells=1:numberofcells;   
    normPoints=length(time);
    endPoint = startPoint + normPoints-1; 
    
    celldata_old=celldata;
    %Normalization
    tmp=        ones(length(celldata(:,1)),1);
    celldatan=  (celldata-tmp*mean(celldata))./(tmp*std(celldata)); %y=(x-mu)/sigma
    celldatanorm=   celldatan - mean(celldatan,2)*ones(1,size(celldata,2)); %normalized = y - artefact signal
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    DATA=   {celldata_old,celldatanorm};
    fig=    figure('NumberTitle','off','Name','PickCells');
    set(gca,'fontsize',23);
    
    %Start picking cells, cell by cell.
    picked_flag=    zeros(1, numberofcells);
    i = 1;
    while i <= numberofcells
        %Plot all cell recordings in one graph.
%         subplot(2,1,1)
%         plot(time,celldata(:,1:end),'b',time,celldata(:,i),'r'); hold on; plot(time, mean(celldata,2),'k','LineWidth',2);
%         set(gca,'Title',text('String','Fluorescence intensity','Color','r'))
%         xlabel('Time (s)')
%         ylabel('Intensity (a u)')
%         set(gca,'Title',text('String',['Cell: ' num2str(i)],'Color','b'))
        figure(fig); clf;
        for iplot=1:2
            celldata=DATA{iplot};
            %Calculate straight line.
        baseLine  = ones(size(celldata(:,1)));
        highLine  = highValue*baseLine;
        meanValue = mean(celldata(startPoint:endPoint,i));

        %Determine normalisation to mean or min.
        [minValue, idxMinValue] = min(celldata(:,i));
        if (meanValue > minValue) 
            caOsc      = celldata(:,i)/abs(minValue);  
            startPoint2 = idxMinValue;
            endPoint2   = idxMinValue;
        end

        %Calculate polynomial trendline.
        trendLine = polyval(polyfit(time,caOsc,trendDegree),time);
        if (meanValue > minValue) && strcmpi(autoNorm, 'yes')
            [minTrend, idxMinTrend] = min(caOsc);
            trendLine = trendLine-(trendLine(idxMinTrend)- minTrend);
        end
        highTrendLine = trendLine+highValue-1;

        %Plot one normalised cell recording in one graph. 
%         subplot(2,1,2)

        subplot(2,1,iplot)
        plot(time,caOsc,'b',time,baseLine,':r',time,highLine,':r',...
           time, trendLine,':m',time,highTrendLine,':m',time(startPoint2:endPoint2),caOsc(startPoint2:endPoint2),'og')
        end
        
        set(gca,'Title',text('String','One Single Cell','Color','r'))
        xlabel('Time (s)')
        ylabel('Intensity (a u)')
        set(gca,'Title',text('String',['Cell: ' num2str(i) ' / ' num2str(numberofcells)],'Color','b'))
                xlabel('Time (s)')
        ylabel('Intensity (a u)')
        
        %Selection buttons.
        uicontrol('Style','pushbutton',...
            'String','Pick',... 
            'Units','normalized',...
            'Position',[0.01 0.01 0.09 0.06],...
            'Callback','pickcb');
        
        uicontrol('Style','pushbutton',...
            'String','Discard',...
            'Units','normalized',...
            'Position',[0.12 0.01 0.09 0.06],...
            'Callback','discardcb');
        
        uicontrol('Style','pushbutton',...
            'String','Back',...
            'Units','normalized',...
            'Position',[0.23 0.01 0.09 0.06],...
            'Callback','backcb');
        
        uiwait

%         disp([num2str(sum(picked_flag)), ' picked out of ', num2str(numberofcells),' cells.'])
        
    end
    close(gcf); clear i;
    picked_indices=find(picked_flag);
    disp(['You picked ', num2str(length(picked_indices)), ' cells.'])
    fprintf('Index of picked cells\n %s\n', num2str(picked_indices));

    file=fopen('pickcells.ini','wt');
    fprintf(file,'%s\n',y{:});
    fclose('all');
    disp('Parameter choice saved in pickcells.ini')
    
    indexofcells=picked_indices;

    csvwrite(fullfile(info.filepath, 'cellpicking.csv'), [indexofcells]);
    
%     save signaldata.txt.dat data_to_save -ascii
    %Clear global variable
    clear picked_indices discardCells i  old_indexofcells picked_flag
    
%If the user presses Cancel.
% else
%     disp('PickCells cancelled! Using the previously selected indices.')
%     y={'1.15','1'};
%     file=fopen('PickCells.ini','wt');
%     fprintf(file,'%s\n',y{:});
%     fclose('all');
% end
end

