function out=getdefault(inifilename)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
fid=fopen(inifilename);
param=[];
if fid~=-1
    for i=1:8
        tempparam=fgetl(fid);
        param=char(param, tempparam);
    end
    cellstr(param);
    defaultanswer1={deblank(param(1,:)), deblank(param(2,:)), deblank(param(3,:)), deblank(param(4,:)), deblank(param(5,:))};
    defaultanswer2={deblank(param(6,:)), deblank(param(7,:)), deblank(param(8,:)), deblank(param(9,:))};
    fclose(fid);
else 
    disp('No previous run! Using default parameters for NetworkIdentification.')
    defaultanswer1={cd,'all','no','0.5','1*10^(-6)'};
    defaultanswer2={'no','1.5','no','0'};
end
out.default1=defaultanswer1;
out.default2=defaultanswer2;
end

