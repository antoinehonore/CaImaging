function frameout= networkid_processframe(iframe,TimeStampFrame,time,celldata,fullcelldata,info,opts)
h=   waitbar(0, {[' ' num2str(iframe) '/' num2str((length(TimeStampFrame)-1))], 'Calculate Correlation matrix, please wait'} );

path_name=info.filepath;
file_name=info.filename;

[T,N]=size(fullcelldata);
indexofcells=opts.netid.idxcells;
nCells=opts.netid.nCells;
samplingtime=opts.netid.samplingtime;
xcoordinates=opts.netid.coordinates.x;
ycoordinates=opts.netid.coordinates.y;
scale=opts.netid.scale;


xcoordinates2=[];
ycoordinates2=[];

t1=TimeStampFrame(iframe);
t2=TimeStampFrame(iframe+1);
if t2>T
    t2=T;
end


%Signals with new time window.
chunktime=time(t1:t2);
chunkcelldata=celldata(t1:t2,:);
chunkfullcelldata=fullcelldata(t1:t2,:);

nchunk= length(chunktime);% delta+1;

%Scramble signals.
%Random rearrangement of time series.

fullcelldata_scrambled=ones(length(chunktime),N);

for i=1:N
    random_start=round(length(chunktime)*rand);
    fullcelldata_scrambled(:,i)=[chunkfullcelldata(random_start+1:end,i); chunkfullcelldata(1:random_start,i)];
end

celldata_scrambled= fullcelldata_scrambled(:,indexofcells);

%--------------------------------------------------------------------------
%Create mean data matrix%
mean_data=[chunktime, chunkcelldata];


%--------------------------------------------------------------------------
%
%
%Set whether the user wants to digitize the signals. 1=yes,
%2=no.
corr_opts=opts.networkid(5:8);
if strcmpi(char(corr_opts(1)),'NO')
    digitize=0;
elseif strcmpi(char(corr_opts(1)),'YES')
    digitize=1;
end

%Set the digitization threshold factor.
threshold=str2num(char(corr_opts(2)));
%Set the lag used in cross covariance correlation. 0=no lag, 1=maximum lag
if strcmpi(char(corr_opts(3)),'NO')
    lagalt=0;
elseif strcmpi(char(corr_opts(3)),'YES')
    lagalt=1;
    disp('Warning! Using maximum lag in covariance function will partially reverse the effect of scrambling!')
else
    lagalt=str2num(char(corr_opts(3)));
end

%Calculate the correlation, distance and lag matrix.
correlationmatrix=ones(nCells);
correlationmatrix_scrambled=ones(nCells);
distancematrix=zeros(nCells);
lagmatrix=zeros(nCells);
lagmatrix_scrambled=zeros(nCells);
celldata_dig=zeros(length(chunktime),nCells);
celldata_dig_scrambled=zeros(length(chunktime),nCells);

%k and l are indices for the matrix, whereas i and j are indices for the
%signaldata in old representation. This is to enable any choice of cell
%indices.
figure(h);
for k=1:nCells
    waitbar(k/nCells,h)
    %         i=old_indexofcells(k);
    y1= chunkcelldata(:,k);
    y1_scrambled=celldata_scrambled(:,k);
    y1_old=y1;
    y1_old_scrambled=y1_scrambled;
    
    for l=k:nCells
        %If the user wants to use lag limited by diffusion time.
        if lagalt == 1
            maxtime=    length(chunktime)/2;
            maxlags=    ceil(maxtime/samplingtime);
            %If the user does not want to use lag.
        else
            maxlags=    0;
        end
        
        y2=chunkcelldata(:,l);
        y2_scrambled=celldata_scrambled(:,l);
        
        if opts.netid.trendcorr.flag==1
            fitpolynom1=polyfit(chunktime,y1,opts.netid.trendcorr.deg);
            fitpolynom1_scrambled=polyfit(chunktime,y1_scrambled,opts.netid.trendcorr.deg);
            y1=y1-polyval(fitpolynom1,chunktime);
            y1_scrambled=y1_scrambled-polyval(fitpolynom1_scrambled,chunktime);
            y1_old=y1;
            y1_old_scrambled=y1_scrambled;
            fitpolynom2=polyfit(chunktime,y2,opts.netid.trendcorr.deg);
            fitpolynom2_scrambled=polyfit(chunktime,y2_scrambled,opts.netid.trendcorr.deg);
            y2=y2-polyval(fitpolynom2,chunktime);
            y2_scrambled=y2_scrambled-polyval(fitpolynom2_scrambled,chunktime);
        end
        
        %If not the same cell.
        
        if k~=l
            if digitize==0 %If the user does not want to digitize.
                %Normal version
                Temp=crosscorrelation(chunktime,y1,y2,maxlags,k,l);
                Rtemp=Temp(1);
                lagtemp=Temp(2);
                lagmatrix(k,l)=lagtemp*samplingtime;
                lagmatrix(l,k)=lagmatrix(k,l);
                correlationmatrix(k,l)=abs(Rtemp);
                correlationmatrix(l,k)=correlationmatrix(k,l);
                
                
                %Scrambled version
                Temp_scrambled=crosscorrelation(chunktime,y1_scrambled,y2_scrambled,maxlags,k,l);
                Rtemp_scrambled=Temp_scrambled(1);
                lagtemp_scrambled=Temp_scrambled(2);
                lagmatrix_scrambled(k,l)=lagtemp_scrambled*samplingtime;
                lagmatrix_scrambled(l,k)=lagmatrix_scrambled(k,l);
                correlationmatrix_scrambled(k,l)=abs(Rtemp_scrambled);
                correlationmatrix_scrambled(l,k)=correlationmatrix_scrambled(k,l);
                
            end
            
            
            %If the user wants to digitize.
            if digitize==1
                Y1=y1; Y2=y2; N=length(chunktime);
                Y1_scrambled=y1_scrambled; Y2_scrambled=y2_scrambled;
                mean1=mean(y1(1:20)); limit1=threshold*mean1; limit12=threshold*mean(Y1);
                mean1_scrambled=mean(y1_scrambled(1:20)); limit1_scrambled=threshold*mean1_scrambled; limit12_scrambled=threshold*mean(Y1_scrambled);
                mean2=mean(y2(1:20)); limit2=threshold*mean2; limit22=threshold*mean(Y2);
                mean2_scrambled=mean(y2_scrambled(1:20)); limit2_scrambled=threshold*mean2_scrambled; limit22_scrambled=threshold*mean(Y2_scrambled);
                YESval=1; NOval=0;
                %Digitize the signals.
                for m=1:N
                    if y1(m)>=limit1
                        y1(m)=YESval;
                    elseif y1(m)<limit1
                        y1(m)=NOval;
                    end
                    if y1_scrambled(m)>=limit1_scrambled
                        y1_scrambled(m)=YESval;
                    elseif y1_scrambled(m)<limit1_scrambled
                        y1_scrambled(m)=NOval;
                    end
                    if y2(m)>=limit2
                        y2(m)=YESval;
                    elseif y2(m)<limit2
                        y2(m)=NOval;
                    end
                    if y2_scrambled(m)>=limit2_scrambled
                        y2_scrambled(m)=YESval;
                    elseif y2_scrambled(m)<limit2_scrambled
                        y2_scrambled(m)=NOval;
                    end
                end
                %If the threshold value was too high or low.
                %For the first signal...
                if (sum(y1)==N*NOval || sum(y1)==N*YESval)
                    %Redo the digitization.
                    for m=1:N
                        if Y1(m)>=limit12
                            y1(m)=YESval;
                        elseif Y1(m)<limit12
                            y1(m)=NOval;
                        end
                    end
                end
                if (sum(y1_scrambled)==N*NOval || sum(y1_scrambled)==N*YESval)
                    %Redo the digitization.
                    for m=1:N
                        if Y1_scrambled(m)>=limit12_scrambled
                            y1_scrambled(m)=YESval;
                        elseif Y1_scrambled(m)<limit12_scrambled
                            y1_scrambled(m)=NOval;
                        end
                    end
                end
                %... or for the second signal.
                if (sum(y2)==N*NOval || sum(y2)==N*YESval)
                    %Redo the digitization.
                    for m=1:N
                        if Y2(m)>=limit22
                            y2(m)=YESval;
                        elseif Y2(m)<limit22
                            y2(m)=NOval;
                        end
                    end
                end
                if (sum(y2_scrambled)==N*NOval || sum(y2_scrambled)==N*YESval)
                    %Redo the digitization.
                    for m=1:N
                        if Y2_scrambled(m)>=limit22_scrambled
                            y2_scrambled(m)=YESval;
                        elseif Y2_scrambled(m)<limit22_scrambled
                            y2_scrambled(m)=NOval;
                        end
                    end
                end
                
                %Calculation of correlation coefficient.
                Temp=crosscorrelation(chunktime,y1,y2,maxlags,k,l);
                Temp_scrambled=crosscorrelation(chunktime,y1_scrambled,y2_scrambled,maxlags,k,l);
                Rtemp=Temp(1); lagtemp=Temp(2);
                Rtemp_scrambled=Temp_scrambled(1); lagtemp_scrambled=Temp_scrambled(2);
                lagmatrix(k,l)=lagtemp*samplingtime;
                lagmatrix_scrambled(k,l)=lagtemp_scrambled*samplingtime;
                lagmatrix(l,k)=lagmatrix(k,l);
                lagmatrix_scrambled(l,k)=lagmatrix_scrambled(k,l);
                correlationmatrix(k,l)=abs(Rtemp);
                correlationmatrix_scrambled(k,l)=abs(Rtemp_scrambled);
                correlationmatrix(l,k)=correlationmatrix(k,l);
                correlationmatrix_scrambled(l,k)=correlationmatrix_scrambled(k,l);
            end
            
            
            %Calculate the distance matrix.
            xx1=xcoordinates(k); yy1=ycoordinates(k);
            xx2=xcoordinates(l); yy2=ycoordinates(l);
            disttemp=sqrt((xx1-xx2)^2+(yy1-yy2)^2);
            distancematrix(k,l)=scale*disttemp;
            
        end
        distancematrix(l,:)=distancematrix(:,l);
    end
    
    
    %Save the digitized signals.
    %If the user wants to digitize the signals.
    if digitize==1
        mean1=mean(y1(1:20)); limit1=threshold*mean1; limit12=threshold*mean(y1);
        mean1_scrambled=mean(y1_scrambled(1:20)); limit1_scrambled=threshold*mean1_scrambled; limit12_scrambled=threshold*mean(y1_scrambled);
        YESval=1; NOval=0;
        %Digitize the signals.
        for m=1:N
            if y1(m)>=limit1
                y1(m)=YESval;
            elseif y1(m)<limit1
                y1(m)=NOval;
            end
            if y1_scrambled(m)>=limit1_scrambled
                y1_scrambled(m)=YESval;
            elseif y1_scrambled(m)<limit1_scrambled
                y1_scrambled(m)=NOval;
            end
        end
        %If the threshold value was too high or low.
        if (sum(y1)==N*NOval || sum(y1)==N*YESval)
            %Redo the digitization.
            for m=1:N
                if y1_old(m)>=limit12
                    y1(m)=YESval;
                elseif y1_old(m)<limit12
                    y1(m)=NOval;
                end
            end
        end
        if (sum(y1_scrambled)==N*NOval || sum(y1_scrambled)==N*YESval)
            %Redo the digitization.
            for m=1:N
                if y1_old_scrambled(m)>=limit12_scrambled
                    y1_scrambled(m)=YESval;
                elseif y1_old_scrambled(m)<limit12_scrambled
                    y1_scrambled(m)=NOval;
                end
            end
        end
        celldata_dig(:,k)=y1;
        celldata_dig_scrambled(:,k)=y1_scrambled;
    end
    
    
    %Prepare for saving the right coordinates.
    xcoordinates2=[xcoordinates2 xcoordinates(1,k)];
    ycoordinates2=[ycoordinates2 ycoordinates(1,k)];
end
%--------------------------------------------------------------------------
%Create mean data matrix%

means=chunkcelldata;
means_with_chunktime=ones(size(means));
[r,c]=size(means);
means_with_chunktime=ones(r,c+1);
means_with_chunktime=[chunktime, means];

mean_data=[means_with_chunktime];


%--------------------------------------------------------------------------
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculate DeltaF/F0 values%

%Define time column%
[m,n]=size(mean_data);
chunktime=mean_data;
chunktime(:,2:n)=[];

%Remove time column from input data
expdata=mean_data(:,2:end);

%Transpose data matrix%
expdata_trans=transpose(expdata);

%Determine number of rows (r) and columns (c)%
[r,c]=size(expdata_trans);

%Calculate DeltaF over F0%
F0=zeros(r,c);
deltaF_trans=zeros(r,c);

for x=1:1:r
    
    %Calculate mean for 20 timepoints before and after (F0)%
    
    for i=21:1:c-20;
        
        F0(x,i)=mean(expdata_trans(x,i-20:i+20));
        
    end
    
    %Calculate deltaF over F0%
    
    
    for i=20:1:c-20
        
        deltaF_trans(x,i)=((expdata_trans(x,i)-F0(x,i))/F0(x,i));
        
    end
    
end

deltaF=transpose(deltaF_trans);
deltaFdata=[chunktime,deltaF];

%Remove uncalculatable
deltaFdata(1:20,:)=[];



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ANTOINE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculate DeltaF/F0 values%
%Remove time column from input data
%     expdata=    chunkcelldata;

%     %Calculate DeltaF over F0%
%     F0=         [];%zeros(nchunk,nCells);
%     deltaF=     [];%zeros(nchunk,nCells);
%
%     wl=         10;
%     for icell=1:nCells %each cell
%         %Calculate mean for 20 timepoints before and after (F0)%
%         j=      0;
%         for i = wl+1:nchunk-wl
%             j=  j+1;
%             F0(j,icell)=mean( expdata(i-wl:i+wl,icell) );
%         end
%
%         j=  0;
%         %Calculate deltaF over F0%
%         for i = wl+1:nchunk-wl
%             j=  j+1;
%             deltaF(j,icell) = ( (expdata(i,icell)-F0(j,icell))/F0(j,icell) );
%         end
%     end
%     deltaFdata=                 [chunktime(wl+1:end-wl),deltaF];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
output.old_indexofcells =   indexofcells;
output.correlationmatrix =  correlationmatrix;
output.distancematrix =     distancematrix;
output.lagmatrix =          lagmatrix;
output.xcoordinates =       xcoordinates2;
output.ycoordinates =       ycoordinates2;
output.celldata =           chunkcelldata;
output.celldata_dig =       celldata_dig;
output.samplingtime =       samplingtime;
output.scrambled=           0;
output.digitized=           digitize;
output.lagused=             lagalt;
output.scale=               scale;

output_scrambled.old_indexofcells =     indexofcells;
output_scrambled.correlationmatrix =    correlationmatrix_scrambled;
output_scrambled.distancematrix =       distancematrix;
output_scrambled.lagmatrix =            lagmatrix_scrambled;
output_scrambled.xcoordinates =         xcoordinates2;
output_scrambled.ycoordinates =         ycoordinates2;
output_scrambled.celldata =             celldata_scrambled;
output_scrambled.celldata_dig =         celldata_dig_scrambled;
output_scrambled.samplingtime =         samplingtime;
output_scrambled.scrambled=             1;
output_scrambled.digitized=             digitize;
output_scrambled.lagused=               lagalt;
output_scrambled.scale=                 scale;


frameout.scrambled=         output_scrambled;
frameout.raw=               output;
frameout.mean=              mean_data;
frameout.deltaf=            deltaFdata;


%Save files
outputfile=         sprintf('%s_%s_%s_%d_%d.mat',file_name(1:end-9),'iddata',opts.userdata{1},t1,t2);
frameout.filen.raw=       outputfile;
frameout.filen.scrambled= [strrep(frameout.filen.raw,'.mat',''), '_scrambled.mat'];
frameout.filen.mean=      [strrep(frameout.filen.raw(1:end-4),'iddata','mean') '.dat'];
frameout.filen.deltaf=    [strrep(frameout.filen.raw(1:end-4),'iddata','deltaF') '.dat'];

% fprintf('\nFiles saved in folder %s\n',fullfile(info.filepath));

save(fullfile(info.filepath,outputfile),'-struct','output')
% fprintf('\t->Correlation:: %s\n', fullfile(outputfile));

save(fullfile(info.filepath,frameout.filen.scrambled),'-struct','output_scrambled')
% fprintf('\t->Scrambled:: %s\n', fullfile(frameout.filen.scrambled));

%Save the mean data%.
save(fullfile(info.filepath,frameout.filen.mean),'mean_data','-ASCII')
% fprintf('\t->Mean:: %s\n',fullfile(frameout.filen.mean));

%Save the DeltaF/F0
save(fullfile(info.filepath,frameout.filen.deltaf),'deltaFdata','-ASCII')

fprintf('\nFiles saved in folder %s\n\t->Correlation:: %s\n\t->Scrambled:: %s\n\t->Mean:: %s\n\t->DeltaF:: %s\n',...
            fullfile(info.filepath),...
            fullfile(outputfile),...
            fullfile(frameout.filen.scrambled),...
            fullfile(frameout.filen.mean),...    
            fullfile(frameout.filen.deltaf));
close(h);
end