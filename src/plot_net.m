function [ ] = plot_net( info )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
cutoff=zeros(1,0);
        allfiles=dir( fullfile(info.filepath,'*_iddata_300_*scrambled.mat') );
        opts.restriction=10^-4;
        
        vopts.LineWidth=3;
        coormat=[];
        
        for ifile=1:length(allfiles)
            
            s_fullfilename= fullfile(allfiles(ifile).folder, allfiles(ifile).name);
            %         strrep(fullfilename,'.mat','_scrambled.mat');
            fprintf(1, 'Now reading %s\n', s_fullfilename);
            s_corrdata= load( s_fullfilename );
            cutoff(ifile) = percentile(   s_corrdata.correlationmatrix( not( s_corrdata.correlationmatrix == 1) )   ,95);
            
        end
        
        opts.avg_cutoff=mean(cutoff);
        
        fprintf('Average cutoff: %f\n',opts.avg_cutoff);
        
        for ifile=1:length(allfiles)
            
            fullfilename= fullfile(allfiles(ifile).folder, strrep(allfiles(ifile).name,'_scrambled',''));
            load(fullfilename);
            
            
            network_fig{ifile}=  figure('Visible', 'On','Name',strrep(allfiles(ifile).name,'_scrambled',''));
            
            plotnetworkcells(correlationmatrix,distancematrix,[xcoordinates; ycoordinates],[xcoordinates; ycoordinates],opts)
            
            figname{ifile}=strrep(fullfilename,'.mat','_network.pdf');
            
                   
            coorvec=correlationmatrix(:);
            distvec=distancematrix(:);
            
            coormat(:,ifile)= coorvec( not(coorvec==1) & distvec < opts.restriction);
            
            number_coeff_above_cutoff(ifile)= sum(coorvec>opts.avg_cutoff);
            
            
        end
        
        mu=mean( coormat );
        sigma=std( coormat);
        
        for ifile=1:length(allfiles)
            figure(network_fig{ifile});
            title({['Mean of the correlation values: ' num2str(mu(ifile))], ['Average Cutoff:' num2str(opts.avg_cutoff)],...
                ['Number of correlating cell pairs:' num2str(number_coeff_above_cutoff(ifile))] }) ;
            set(gca,'fontsize',23);
            saveas( network_fig{ifile}, figname{ifile},'pdf') ;
        end

end

