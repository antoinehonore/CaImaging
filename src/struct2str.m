function [s]= struct2str(structure)
fields= fieldnames(structure);
s='';
for ifield=1:length(fields)
if isnumeric(structure.(fields{ifield}))
    s=[s fields{ifield} num2str(structure.(fields{ifield})) '_'];
elseif isstruct(structure.(fields{ifield}))
    s=[s, (fields{ifield}) '__' struct2str( structure.(fields{ifield}) ) '__'];
end
end

% if isstruct(structure) && ~isempty(fieldnames(structure))
%     fields= fieldnames(structure);
% %     if isstruct()
%     val= struct2cell(structure);
%     
%     s= [strcat( strcat(fields,''), cellfun(@num2str,val,'UniformOutput',false), '_' )]';
%     s= strcat(s{:});
% else
%     s='';
% end
end
