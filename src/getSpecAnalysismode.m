function [opts]= getSpecAnalysismode
% Display dialog boxes to manually enter options for the Spectral analysis.
% Default parameter values will be suggested and are contained in the files
% *.ini
global pow
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DIALOGBOX, spectral analysis
% Open and read init-file for usage of latest input values
pow.file.init = 'standard.ini';
if ~readFile('init')
    errordlg('File not found','File Error');
end
title  = 'Spectral Analysis of Calcium Oscillations by Per Uhlen';
prompt = {'Enter the name of the target file',...
    'Number of FFT bins (in power of 2)',...
    'Number of peaks to store (1,2,... OR auto)',...
    'Sort/store peaks by (area OR height)',...
    'Filter (no (rectangular), Hanning, Hamming, Blackman OR Bartlett)',...
    'Trend correction degree (no = 0 OR 1 to 4)'};
pow.dialog.answer = inputdlg(prompt,title,1,pow.dialog.def);
pow.file.target = char(pow.dialog.answer(1));

pow.N           = 2^str2num(char(pow.dialog.answer(2)));
if strcmpi(char(pow.dialog.answer(3)),'auto')
    pow.autoNoPeaks = 1;
    pow.noPeaks = 1;
else
    pow.autoNoPeaks = 0;
    pow.noPeaks = str2num(char(pow.dialog.answer(3)));
end
pow.areaSelect  = char(pow.dialog.answer(4));
pow.filter      = char(pow.dialog.answer(5));
pow.trendDegree     = str2num(char(pow.dialog.answer(6)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set increment of the power spectrum
% This variable determines how many values that will be saved in
% the power spectrum file. If set to 1 all values are saved
pow.increment = 1;

% - - - - Don't change anything beyond this point - - -  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Store user defined values
writeFile('init');

% Check user input
if ~strfind(pow.file.target,'.')
    h = warndlg('Target file must have a file extension (.txt)','!! Warning !!');
    uiwait(h);
end

% Check user input
if strcmpi(pow.areaSelect,'area')
    pow.areaSelect = 1;
elseif strcmpi(pow.areaSelect,'height')
    pow.areaSelect = 0;
else
    h = warndlg('Sort peaks? area OR height','!! Warning !!');
    uiwait(h);
    
end

% Check user input
if strcmpi(pow.filter,'hanning')
    pow.filter = 'Hanning';
elseif strcmpi(pow.filter,'hamming')
    pow.filter = 'Hamming';
elseif strcmpi(pow.filter,'blackman')
    pow.filter = 'Blackman';
elseif strcmpi(pow.filter,'bartlett')
    pow.filter = 'Bartlett';
elseif strcmpi(pow.filter,'no')
    pow.filter = 'No';
else
    h = warndlg('Filter? No filter will be used!','!! Warning !!');
    uiwait(h);
    pow.filter = 'No';
end


opts.pow=pow;

end

