function netquant(info,opts)
% Specify the folder where the files live.save_directory='C:\Users\David Forsberg\Documents\Mikroskop\Calcium Imaging\Analyser och filmer\Analysis data\Quantification\';
% load folderinfo.mat
%
% infilelocation=outfilelocation;
% outfilelocation=[infilelocation '\Quantification\'];
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
% % Check to make sure that folder actually exists.  Warn user if it doesn't.
% if ~isdir(infilelocation)
%     errorMessage = sprintf('Error: The following folder does not exist:\n%s', infilelocation);
%     uiwait(warndlg(errorMessage));
%     return;
% end
addpath(genpath('matlab_bgl'))
filePattern = fullfile(info.filepath,[ '*iddata*' opts.userdata{1} '*.mat']); % Change to whatever pattern you need.

theFiles = dir(filePattern);
%-------------------------------------------------------------------------%
%Set Cutoff values with scrambled files%
%-------------------------------------------------------------------------%




% userdata1=inputdlg({'Correlation cutoff?'},'');
% opts.netquant.cutoff.corr=str2num(userdata1{1});
% userdata2=inputdlg({'Distance restriction?'},'');
% opts.netquant.cutoff.dist=str2num(userdata2{1});




for k = 1 : length(theFiles)
    baseFileName = theFiles(k).name;
    FileName = fullfile(info.filepath, baseFileName);
    
    if not( contains(FileName, 'scrambled') ) %If the file is not scrambled
        %% Process the scrambled version to find the cutoff value
        s_FileName= strrep(FileName,'.mat','_scrambled.mat');
 %       fprintf(1, 'Now reading %s\n', FileName);
        s_corrdata= load( s_FileName );
        opts.netquant.cutoff.corr = percentile(   s_corrdata.correlationmatrix( not( s_corrdata.correlationmatrix == 1) )   ,95);
%         opts.netquant.cutoff.dist = 10^6*percentile(   s_corrdata.distancematrix(    not( s_corrdata.distancematrix    == 0) )   ,95);
        fprintf('Cutoff correlation: %f\n',opts.netquant.cutoff.corr);
        clear s_corrmat s_distmat s_corrdata s_FileName;
        
        %% Do normal file processing
        fprintf(1, 'Now reading %s\n', FileName);
        corrdata= load(FileName);
        
        correlationmatrix=  corrdata.correlationmatrix;
        distancematrix=     10^6*corrdata.distancematrix;
        
        [T,N]=size(corrdata.celldata);
        
        scrambled=corrdata.scrambled;
        if scrambled==1
            warning('You are analyzing scrambled data!')
        end
        
        %-------------------------------------------------------------------------%
        %Generate Connectivity matrix%
        %-------------------------------------------------------------------------%
        
        icorrelated=    (correlationmatrix >= opts.netquant.cutoff.corr) & (correlationmatrix ~=1);
%         iclose=         distancematrix <= opts.netquant.cutoff.dist;
        iconnected=     icorrelated;  %& iclose;
        
        
        CorrelationVal=nan(size(correlationmatrix));
        CorrelationVal(icorrelated)= correlationmatrix(icorrelated);
        CorrelationValwithoutNaN=CorrelationVal(:);
        CorrelationValwithoutNaN(isnan(CorrelationValwithoutNaN))=[];
        
        
        %-------------------------------------------------------------------------%
        %Statistics%
        %-------------------------------------------------------------------------%
        if ~isempty(CorrelationValwithoutNaN)
            meancorrelation = mean(CorrelationVal(:),'omitnan');
            mediancorrelation = percentile(CorrelationValwithoutNaN,50);
            percentile99 = percentile(CorrelationValwithoutNaN,99);
            percentile99_all = percentile(correlationmatrix,99);
            
            
            
            %-------------------------------------------------------------------------%
            %calculate number of correlating cell pairs%
            %-------------------------------------------------------------------------%
            number_of_correlations=sum(icorrelated(:))/2;
            [d,f]=size(iconnected);
            
            number_of_possible_correlations=d*f;
            percentofpairsconnected=number_of_correlations/number_of_possible_correlations;
            
            %-------------------------------------------------------------------------%
            %calculate "MatLab Statistics" (from NetworkAnalysis)%
            %-------------------------------------------------------------------------%
            
            %Make an adjacency matrix from correlation matrix.
            adj_matrix=zeros(N);
            for i=1:N
                for j=1:N
                    if correlationmatrix(i,j)>opts.netquant.cutoff.corr
                        adj_matrix(i,j)=1;
                    end
                end
            end
            %Calculate connectivity.
            n=0;
            for i=1:N(1)
                if sum(adj_matrix(:,i))==1
                    n=n+1;
                end
            end
            connectivity=(N(1)-n)/N(1);
            
            %Make an adjacency matrix from correlation matrix.
            adj_matrix=zeros(N, N);
            for i=1:N
                for j=1:N
                    if correlationmatrix(i,j)>opts.netquant.cutoff.corr
                        adj_matrix(i,j)=1;
                    end
                end
            end
            %Scramble the adjacency matrix.
            scrambled=adj_matrix;
            random_start=round(rand*N);
            for i=1:N
                scrambled(:,i)=[scrambled(random_start+1:end,i); scrambled(1:random_start,i)];
            end
            %Sparse representation
            adj_matrix=sparse(adj_matrix);
            scrambled=sparse(scrambled);
            %Shortest path length.
            L1=all_shortest_paths(adj_matrix);
            L2=all_shortest_paths(scrambled);
            length1=[];
            for i=1:N
                for j=1:N
                    if L1(i,j)<1000000
                        length1=[length1 L1(i,j)];
                    end
                end
            end
            length2=[];
            for i=1:N
                for j=1:N
                    if L2(i,j)<1000000
                        length2=[length2 L2(i,j)];
                    end
                end
            end
            lambda=mean(length1)/mean(length2);
            %Clustering coefficient.
            C1=clustering_coefficients(adj_matrix);
            C2=clustering_coefficients(scrambled);
            Cnet1=mean(C1);
            Cnet2=mean(C2);
            sigma=Cnet1/Cnet2;
            %Degree distribution.
            degree_vector=zeros(1,N);
            for i=1:N
                degree_vector(1,i)=sum(adj_matrix(i,:))-1;
            end
            max_deg=max(degree_vector);
            deg_distribution=[];
            degrees=[];
            for i=1:max_deg
                if find(degree_vector==i)
                    deg_distribution=[deg_distribution length(find(degree_vector==i))/N];
                    degrees=[degrees i];
                end
            end
            
            % %-------------------------------------------------------------------------%
            % %Generate result%
            % %-------------------------------------------------------------------------%
            %
            % result=[old_indexofcells;sum(connectivitymatrix(2:r,1:c))-1;(sum(connectivitymatrix(2:r,1:c))-1)/N];
            % statistics=[mean(correlationvector1);mean(correlationvector2);percentile(correlationvector1,50);percentile(correlationvector2,50);percentile(correlationvector1,99);percentile(correlationvector2,99);number_of_correlations;percentofpairsconnected];
            
            %-------------------------------------------------------------------------%
            %Display result%
            %-------------------------------------------------------------------------%
            
            %         fprintf('\n');
            %         fprintf('Analyzed file: %s \n',FileName);
            %         fprintf('Cutoff: %f \n',opts.netquant.cutoff.corr);
            %         fprintf('Number of correlations: %f \n',number_of_correlations);
            %         fprintf('Number of active cells: %f \n',N);
            %         fprintf('Mean correlation (above cutoff): %f \n',meancorrelation);
            %         fprintf('Median correlation (above cutoff): %f \n',mediancorrelation);
            %         fprintf('99th percentile: %f \n',percentile99);
            %         fprintf('99th percentile (all): %f \n',percentile99_all);
            %         fprintf('Connectivity: %f \n',(connectivity));
            %         fprintf('Lambda: %f \n',(lambda));
            %         fprintf('Sigma: %f \n',(sigma));
            
            %-------------------------------------------------------------------------%
            %Save result%
            %-------------------------------------------------------------------------%
            
            file_nameQuantification= sprintf('%s_%s',baseFileName(1:end-4),'quantification');

            results_str=sprintf('Analyzed file: %s\nCutoff: %f\nNumber of correlations: %d\nNumber of active cells: %d\nMean correlation (above cutoff): %s\nMedian correlation (above cutoff): %f\n99th percentile: %f\n99th percentile (all): %f\nLambda: %f\nSigma: %f\nConnectivity: %f\n',...
				 baseFileName,opts.netquant.cutoff.corr,number_of_correlations,N, meancorrelation, mediancorrelation, percentile99,percentile99_all, lambda, sigma, connectivity);
            
            fout= fopen(fullfile(info.filepath,[file_nameQuantification '.txt']), 'w');
            fprintf(results_str);
            fprintf(fout, results_str);
           fclose(fout);
         

            disp('Quantification results saved');
        else
            warning('No correlation found in file %s\n',baseFileName);
        end
        
    end
end
