%-------------------------------------------------------------------------
%
% Code from NetworkIdentification_x
% Karolinska Institutet, Stockholm, Sweden, 2017-04-10
% Antoine Honore
%--------------------------------------------------------------------------
function [out, matfilename]= networkidentification(info,opts)
%function [out, matfilename]= networkidentification(info,opts)
% Perform the network identification from the data file, and for the
% different time period defined by the user.

file_path=info.filepath;
file_name=info.filename;
% open the datafile
fid1 = fopen( fullfile(file_path, file_name) );
if fid1==-1
    error(['Problem opening ' fullfile(file_path, file_name)]);
end

%Read the first line containing the labels
line=   fgetl(fid1); line2=strsplit(line,'\s*',...
    'DelimiterType','RegularExpression');

%Number of cells
N=sum( contains(line2, 'Mean'));

%Extract data
expdata=fscanf(fid1,'%f',[length(line2) Inf])';

%Normalised time points.
time=(expdata(:,1)-expdata(1,1));

%Number of frames
T=length(time);

fullcelldata=       expdata(:,2:3:end);
fullxcoordinates=   expdata(:,3:3:end);
fullycoordinates=   expdata(:,4:3:end);


fprintf('Oscillation data was loaded from: \n\t%s\n',fullfile(file_path, file_name));


%Interprete picking cell option
%Set the indices of the cells that should be analysed.
answer1=        opts.networkid;

c1=opts.networkid{1};


if strcmpi(c1,'picked')  % If user wants to pick cells
    
    %  Look for existing pickeing file
    if exist(fullfile(file_path, 'cellpicking.csv'), 'file') == 2
        [num] = csvread( fullfile(file_path, 'cellpicking.csv') );
        indexofcells = num(end,:);
        
        %  User wants to pick cells or not.
        if strcmpi(char(answer1(2)),'YES') 
            indexofcells = pickcells(fullcelldata(:,indexofcells),info,opts);
        end
    
    else
        warning('No cellpicking file, Go to pickcells routine');
        indexofcells=pickcells(fullcelldata,info,opts);
    end

elseif strcmpi(c1,'RN') || strcmpi(c1,'ASTRO') || strcmpi(c1,'NONASTRO')
    
    fid=fopen(fullfile(file_path, sprintf('%s.txt',char(answer1(1)))),'r');
    if fid == -1
        error('Cannot open rn.txt file');
      
    else
       fgetl(fid);
        formatSpec = '%f';
        sizeA = [5 Inf];
        
        num = fscanf(fid,formatSpec,sizeA)';
        fclose(fid);
        %     [num]= xlsread( fullfile(path_name, 'rn.xlsx') );
        index_of_particularcells=num( num(:,3)~=0,1 )';
        
        %Compare with the picked cells
        try
            [num]=csvread( fullfile(file_path, 'cellpicking.csv') );
            index_of_pickedcells= num(end,:);
        catch
            warning('No cellpicking file, Go to pickcells routine');
            index_of_pickedcells=pickcells(fullcelldata,info,opts);
        end        
    end

elseif strcmpi(c1,'ALL')
    if strcmpi(char(answer1(2)),'YES') %  User wants to pick cells or not.
        indexofcells=pickcells(fullcelldata,info,opts);   
    else
        indexofcells= 1:N;
    end

else%If we entered numbers
    indexofcells= str2num(c1);
end

if ~exist('indexofcells','var')
    %If we chose particular cells, we must match them with the picked cells
    try
        tmp=zeros(2,N);
        tmp(1,index_of_pickedcells)=1;
        tmp(2,index_of_particularcells)=1;
        indexofcells= find( sum(tmp) == 2);
%         indexofcells=index_of_particularcells;
    catch
        error('Problem in the choice of cells');
    end
end
if isempty( indexofcells )
    error('/!\ 0 cell picked');
end

fprintf('Index of picked cells\n %s\n', num2str(indexofcells));

old_indexofcells=indexofcells;


celldata=       fullcelldata(:,indexofcells);
xcoordinates=   mean(fullxcoordinates(:,indexofcells));
ycoordinates=   mean(fullycoordinates(:,indexofcells));
opts.netid.nCells=         length(indexofcells);
opts.netid.coordinates.x=   xcoordinates;
opts.netid.coordinates.y=   ycoordinates;

%Samplingtime in seconds. How often a picture is taken.
opts.netid.samplingtime=   str2num(char(answer1(3)));
%Set the length opts.netid.scale used in the experiment. Physical distance between two
%pixles in meters.
opts.netid.scale=          str2num(char(answer1(4)));

%Convert time points to seconds.
time=           opts.netid.samplingtime*time;

% delta=inputdlg({'Analysis section length (sec)'},'');
user_defined_frames=str2num(opts.delta{1})/opts.netid.samplingtime;%Convert from seconds to samples

if length(user_defined_frames)==1
    delta=          user_defined_frames;

if (delta==0)
    TimeStampFrame= [1 T];
else
    TimeStampFrame= [1:delta:T];
    if (T - TimeStampFrame(end))/delta > .5
        TimeStampFrame(end+1)=T;
    end
    
end
else
    TimeStampFrame= [1 cumsum(user_defined_frames)];
end

%--------------------------------------------------------------------------
% Correlation opts interpretation
%--------------------------------------------------------------------------
%Set whether trendcorrection should be implemented and used or not in a
answer2=opts.networkid(5:8);
if strcmpi(char(answer2(4)),'0')
    opts.netid.trendcorr.flag=0;
else
    opts.netid.trendcorr.flag=1;
    %Set the degree of polynomial that should be used for trendcorrection.
    opts.netid.trendcorr.deg=str2num(char(answer2(4)));
end
opts.netid.idxcells=indexofcells;

for iframe=1:(length(TimeStampFrame)-1)
    
    out{iframe}=networkid_processframe(iframe,TimeStampFrame,time,celldata,fullcelldata,info,opts);
    
end

% close(h); %Close waitbar

matfilename= [info.filename(1:end-9) '_netid_' opts.userdata{1} '_' num2str(length(out)) 'b.mat'];
netidout=out;


%SAVE RESULTS IN MATLAB STRUCTURE
save(fullfile(info.filepath, matfilename),'netidout','info','opts');

end