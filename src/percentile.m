%A method that calculates the p:th percentile of an unsorted vector.

function y=percentile(vector,p)
    vector=sort(vector);
    n=p/100*length(vector);
    y=vector(floor(n)+1);