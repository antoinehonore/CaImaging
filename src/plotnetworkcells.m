function plotnetworkcells(corrmatrix, distmatrix,row_coor,col_coor,opts)


% cutoff=.8;
maxwidth=3;
restriction=opts.restriction;
cutoff=opts.avg_cutoff;

indexmatrix1=[];

[nrow, ncol]=   size(corrmatrix);
%         title(['Distance restriction: ', num2str(restriction), ' microns.'])

for j=1:ncol
    for i=1:nrow
        indexmatrix1=[indexmatrix1; i j];
    end
end

corrvector=corrmatrix(:);
[corrvector, oldindex]=sort( corrvector  );
distvector=distmatrix(:);
distvector=distvector(oldindex);

indexmatrix2=indexmatrix1(oldindex,:);


color_chosen=colormap( jet );

colorsize=size(color_chosen);
M=512;%Image size

%Set the maximum width, with which the cell network is plotted.

h=waitbar(0,'Please wait... Plotting correlation network');
fprintf('number of coefficients above threshold: %d\n',sum(corrvector>cutoff));
for icell=1:length(corrvector)
    waitbar(icell/length(corrvector))
    %     for iastro=1:nrow
    
    corrcoff=  corrvector(icell);
    
    if corrcoff>cutoff 
        %                 colorval= color_chosen( find(corrcoff==corrvector,1) ,:);
        indextransform=1-(1-corrcoff)/(1-cutoff);
        colorindex=ceil(indextransform*colorsize(1));
        colorval=color_chosen(colorindex,:);
        width=maxwidth*corrcoff;
        %Plot lines between different cells, but not the same.
        
        
        if distvector(icell) <= restriction
            line([col_coor(1, indexmatrix2(icell,2)) row_coor(1,indexmatrix2(icell,1))],...
                [col_coor(2,indexmatrix2(icell,2)) row_coor(2,indexmatrix2(icell,1))],...
                'color',colorval,'linewidth',width); hold on;
        end
        
    end
    %     end
end
colorbar
caxis([cutoff 1])
close(h);

end

