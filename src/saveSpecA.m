function saveSpecA(freq,filedata,info)

exp_name=info.filename(1:end-9);
s=fieldnames(freq{1,1});
lname={exp_name; s{3}; s{4}; s{5}; s{6};'Number of valid cells';'Date'};
filedata(1,:)=strrep(strrep(filedata(1,:),sprintf('%s_',exp_name),''),'.dat','');

dates= cell(1,size(filedata,2)) ;
dates(:)= {datestr(datetime('now'))};
ncellsLine=[];
for i=1:size(freq,1)
    for j=1:size(freq,2)
        ncellsLine{i,j}= length(freq{i,j}.vec);
    end
end

% printdata=      [lname, [filedata; cellfun(@num2str,ncellsLine(:)','UniformOutput',false); dates]];
printdata=      [lname, [filedata; ncellsLine(:)'; dates]];

if not( contains(computer,'MAC') ) %If Windows or Linux, print in xlsfile
    outfile= [info.outfile '.xlsx'];
    try
        [~,~,raw]=xlsread( fullfile( info.datapath, outfile) );
    catch
        raw=lname;
    end
    xlswrite(fullfile( info.datapath, outfile), [printdata] );
    fprintf('\nResults in %s\n\n',fullfile( info.datapath, outfile));
end


%Print in a dat file anyways, with Append mode (add lines below existing
%ones)
outfile= [info.outfile '.txt'];
fid=            fopen(fullfile( info.datapath, outfile),'a');


[nrow,ncol]=    size(printdata);
formatSpecfullstring= '%s\t ';
formatSpecbody= '%s\t ';

for icol=2:ncol
    formatSpecfullstring=[formatSpecfullstring, '%s\t '];
    formatSpecbody=[formatSpecbody, '%f\t '];
end

formatSpecfullstring=[formatSpecfullstring ,'\n'];
formatSpecbody=[formatSpecbody ,'\n'];
%First line
fprintf(fid,formatSpecfullstring,printdata{1,:});

%data lines
for irow=2:nrow-1
    fprintf(fid,formatSpecbody, printdata{irow,:});
end

%Last line (printing date)
fprintf(fid,formatSpecfullstring, printdata{end,:});
fclose(fid);


fprintf('\nResults in %s\n\n',fullfile( info.datapath, outfile));
end
