clear
load folderinfo.mat

infilelocation=[outfilelocation 'Quantification\'];
outfilelocation=infilelocation;

% filenameinput=inputdlg({'Result file name'},'');
% nameofresultfile=filenameinput{1};
% resultfile=[outfilelocation nameofresultfile];

% Check to make sure that folder actually exists.  Warn user if it doesn't.
if ~isdir(infilelocation)
    errorMessage = sprintf('Error: The following folder does not exist:\n%s', infilelocation);
    uiwait(warndlg(errorMessage));
    return;
end

EXPDATE=inputdlg({'What do you analyze?'},'');
expdate=EXPDATE{1};

if strcmp(expdate,'all')
    filePattern = fullfile(infilelocation, ['*.mat']); % Change to whatever pattern you need.
    
else
    % Get a list of all files in the folder with the desired file name pattern.
    filePattern = fullfile(infilelocation, [expdate '*quantification.mat']); % Change to whatever pattern you need.
end
theFiles = dir(filePattern);
X=[];
for k = 1 : length(theFiles)
    baseFileName = theFiles(k).name;
    FileName = fullfile(infilelocation, baseFileName);
    load (FileName);
    eval(['x= ' matlab.lang.makeValidName( baseFileName(1:end-4)) ';']);
    eval(['clear ' matlab.lang.makeValidName( baseFileName(1:end-4)) ]);
    if isempty(X);
        X=x;
    else
        X=[X,x(:,2)];
    end
    
end
% resultvariables=who('x*');


% xlswrite(resultfile, resultvariables{:});
