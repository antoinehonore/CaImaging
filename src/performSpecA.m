function freq = performSpecA(A, filename, info,opts)
% function freq = performSpecA(A, filename, info,opts)
%

global pow

pow=                opts.pow;
pow.file.source=    filename;
% Determine number of cells
[~,pow.noCells] =   size(A);
pow.noCells     =   pow.noCells-1;

pow.file.target=    fullfile(info.filepath, [filename(1:end-4) '_peaks.txt']);


% Check user input
if pow.noCells < 1
    h = warndlg('This file appears not to have any calcium recordings','!! Warning !!');
    uiwait(h);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - - - - - - Variables that can be changed - - - - - -  %

%Above the cutoff, the frequencies do not correspond to a signal from a
%cell
peakcutoff=200;%mHz


% Set the accepted time error in seconds for each sample
% This variable checks that the time steps are evenly spaced
timeError = 0.25;

% Set the lowest accepted frequency in periods to be analyzed
% This variable excludes frequencies lower than lowPeriod periods
lowPeriod = 5.0;

% Set the level for auto-storage of the strongest peaks
% This variable stores peaks with powers greater than autoLevel
% of the highest/strongest peak.
autoLevel = 0.75;

%Perform the actual analysis

% Extract time vector and calculate time step (dt)
pow.timeVec = A(:,1);
pow.dt      = mean(diff(pow.timeVec));

% Check that time steps are equal
% timeError is the accepted time difference
if ~isempty( find(abs(diff(pow.timeVec)-pow.dt) > timeError,1) )
    h = warndlg('Not equal time steps','!! Warning !!');
    uiwait(h);
end

% -*- Key -*-
% Determine sample frequency, frequency resolution and Nyquist frequency
% mHz is the unit

mHz            = 1e3;
pow.sampleFreq = mHz*1/pow.dt;
pow.nyquist    = mHz*1/(2*pow.dt);
pow.freqVec    = pow.sampleFreq*(0:pow.N/2)/pow.N;
pow.B          = pow.freqVec';

% The most dominant frequencies are stored in pow.freq.vec
pow.freq.vec = [];

% Write header in target file
if ~writeFile('header')
    errordlg('File not found','File Error');
    
end

% % On-screen information
% fprintf('The file %s contain %g graphs\n',pow.file.source,pow.noCells)
% h = helpdlg('Select region and/or press key to analyze. Press n to skip to next!',...
%     'Region Selection');
% uiwait(h);

% Open a new figure window
figure('NumberTitle','off','Name','Spectral Analysis by Per Uhlen');

% Start spectrum analysis cell by cell
for col = 2:pow.noCells+1
    pow.counter = col;
%     fprintf('%5i (%i)\r',pow.counter-1,pow.noCells);
    
    % Extract single cell calcium recording to analyze
    y = A(:,pow.counter);
    
    % -*- Key -*-
    % Trend elimination and centering of graph
    if pow.trendDegree > 0
        pow.trend = polyfit(pow.timeVec,y,pow.trendDegree);
        y         = y - polyval(pow.trend,pow.timeVec);
    end
    
    % Plot original single cell calcium recording
    subplot(2,1,1), cla
    plot(pow.timeVec,y)
    ylabel('Fluorecent Intensity')
    
    % Select area of interest to analyze
    t1       = 1;
    t2       = length(pow.timeVec);
    
%     point1    = t1;
%     point2    = t2;
%     x1        = min(point1(1,1),point2(1,1));
%     x2        = max(point2(1,1),point2(1,1));
    
%     for t=1:length(pow.timeVec)-1
%         if x1 >= pow.timeVec(t) && x1 < pow.timeVec(t+1)
%             t1 = t;
%         end
%         if x2 > pow.timeVec(t) && x2 <= pow.timeVec(t+1)
%             t2 = t;
%         end
%     end

    subplot(2,1,1), cla
    plot(pow.timeVec(t1:t2),y(t1:t2))
    ylabel('Fluorecent Intensity')

    % Save the last key pressed
    pow.lastKey = get(gcf,'CurrentCharacter');
    
    % Set new y and temporary tmpTimeVec
    y          = y(t1:t2);
    tmpTimeVec = pow.timeVec(t1:t2);
    
    % Trend elimination and centering of new graph
    if pow.trendDegree > 0
        pow.trend2 = polyfit(tmpTimeVec,y,pow.trendDegree);
        y          = y - polyval(pow.trend2,tmpTimeVec);
    end
    
    % Filtering the signal
    if ~(strcmpi(pow.filter,'No') || strcmpi(pow.filter,'Rectangular'))
        filtTimeVec = (tmpTimeVec-tmpTimeVec(1))/(tmpTimeVec(end)-tmpTimeVec(1));
        if strcmpi(pow.filter,'Hanning')
            filt = 0.5-0.5*cos(2*pi*filtTimeVec);
        elseif strcmpi(pow.filter,'Hamming')
            filt = 0.54-0.46*cos(2*pi*filtTimeVec);
        elseif strcmpi(pow.filter,'Blackman')
            filt = 0.42-0.5*cos(2*pi*filtTimeVec)+0.08*cos(4*pi*filtTimeVec);
        elseif strcmpi(pow.filter,'Bartlett')
            tCenter = ceil(length(filtTimeVec)/2);
            upRamp  = filtTimeVec(1:tCenter)/filtTimeVec(tCenter);
            if rem(length(filtTimeVec),2) == 0
                filt = [upRamp; flipud(upRamp(1:end))];
            else
                filt = [upRamp; flipud(upRamp(2:end))];
            end
        end
        y = y.*filt;
    end
    
    % Plot the new trend corrected and centered time series
    subplot(2,1,1), cla
    plot(tmpTimeVec,y)
    ylabel('Fluorecent Intensity')
    
    % Plot the filter function
    if ~strcmpi(pow.filter,'No')
        subplot(2,1,1), hold on
        plot(tmpTimeVec,filt,':m')
    end
    
    % -*- Key -*-
    % Fast Fourier Transform
    Y = fft(y,pow.N);
    
    % -*- Key -*-
    % Power spectral density (the Power Spectrum)
    % The factor 2 compensates for missing negative values
    P            = Y.*conj(Y)/pow.N;
    P            = P(1:pow.N/2+1);
    P(2:pow.N/2) = 2*P(2:pow.N/2);
    pow.B        = [pow.B P];
    
    % -*- Key -*_
    % Determine all frequency peaks
    dP        = gradient(P);
    allPeaksP = find(dP(1:end-1)>0 & dP(2:end)<0);
    for peakN = 1:length(allPeaksP)
        if P(allPeaksP(peakN))<P(allPeaksP(peakN)+1)
            allPeaksP(peakN) = allPeaksP(peakN)+1;
        end
    end
    allValleysP = find(dP(1:end-1)<0 & dP(2:end)>0);
    for valleyN = 1:length(allValleysP)
        if P(allValleysP(valleyN))>P(allValleysP(valleyN)+1)
            allValleysP(valleyN) = allValleysP(valleyN)+1;
        end
    end
    allValleysP = [1; allValleysP];
    pow.peaks   = [pow.freqVec(allPeaksP)' P(allPeaksP)];
    
    % Exclude frequencies lower than lowPeriod periods
    timeSpan  = tmpTimeVec(end)-tmpTimeVec(1);
    pow.peaks = pow.peaks(find(pow.peaks(:,1)>lowPeriod*1e3/timeSpan),:);
    
    % -*- Key -*_
    % Store analyzed data in pow.peaks with three columns
    % Frequency PSDheight RelativePower(in %)
    areaP = trapz(pow.freqVec,P);
    for j = 1:size(pow.peaks,1)
        [~,I] = sort(abs(pow.freqVec(allValleysP)-pow.peaks(j,1)));
        if length(allValleysP) > 1
            lowLim  = min(allValleysP(I(1:2)));
            highLim = max(allValleysP(I(1:2)));
        else
            lowLim  = 1;
            highLim = length(pow.freqVec);
        end
        pow.peaks(j,3) = 100*trapz(pow.freqVec(lowLim:highLim),P(lowLim:highLim))/areaP;
    end
    
    % Sort peaks depending on relative power or PSD height
    if pow.areaSelect
        pow.peaks = flipud(sortrows(pow.peaks,3));
    else
        pow.peaks = flipud(sortrows(pow.peaks,2));
    end
    
    % Automatically stores all the highest peaks determined by autoLevel
    if pow.autoNoPeaks
        if pow.areaSelect
            try
                pow.noPeaks = size(find(pow.peaks(:,3)>autoLevel*pow.peaks(1,3)),1);
            catch %In cse there is no peak at a higher frequency than 1/lowPeriod
                pow.noPeaks=0;
            end
        else
            try
                pow.noPeaks = size(find(pow.peaks(:,2)>autoLevel*pow.peaks(1,2)),1);
            catch %In cse there is no peak at a higher frequency than 1/lowPeriod
                pow.noPeaks=0;
            end
        end
    end
    
    if pow.noPeaks>0
        % Reduce the pow.peaks matrix to pow.noPeaks
        pow.peaks = pow.peaks(1:pow.noPeaks,:);

    
        % Store the most dominant frequency for mean calculation
        pow.freq.vec = [pow.freq.vec pow.peaks(1,1)];
    end

    % Write analyzed data to file
    if ~writeFile('data')
        errordlg('File not found','File Error');
        
    end
    
  % Plot power spectrum with text
  subplot(2,1,2), cla
  plot(pow.freqVec,P)
  set(gca,'Title',text('String','Power Spectrum','Color','r'))
  xlabel('Frequency (mHz)')
  ylabel('Power Spectral Density')
    
end
pow.freq.fullvec=   pow.freq.vec;


% 
fid_freq=fopen( fullfile(info.filepath,'freqmatrix2.txt'),'a' );
fprintf(fid_freq, '%f\n', pow.freq.vec); fprintf(fid_freq,'\n');
fclose(fid_freq);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%THRESHOLD
idiscardedP= pow.freq.vec > peakcutoff;
discardedP=  find(idiscardedP);
pow.freq.vec= pow.freq.vec(~idiscardedP);

if ~isempty(discardedP)
flog=fopen(pow.file.target,'a');
logstr=sprintf('%%---\n%s:: Deleted peak in %d/%d cell(s): %s\n%%---\n',...
     datestr(datetime('now')), sum(idiscardedP), pow.noCells, num2str(discardedP));
fprintf('%s',logstr);
fprintf(flog,'%s',logstr);
fclose(flog);
 end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


text(pow.nyquist,0,[' \leftarrow Nyquist = ',num2str(pow.nyquist,4),' mHz'],...
    'FontSize',9,'Rotation',90,'Color','r');
text(lowPeriod*1e3/timeSpan,0,' \leftarrow','FontSize',9,'Rotation',90,'Color','g');


% Statistics
pow.freq.meanValue = mean( pow.freq.vec);
% if pow.noCells > 1
    pow.freq.semValue = std(pow.freq.vec)/sqrt(pow.noCells-1);
    pow.freq.StD = std(pow.freq.vec);
    pow.freq.CoV = std(pow.freq.vec)/mean(pow.freq.vec);
    
if pow.noCells<=1
    warning('/!\ Less than 2 cells analyzed');
end
freq=pow.freq;

% Write info on the screen
fprintf('Mean frequency: %f mHz \n',pow.freq.meanValue);
fprintf('SEM: %f mHz \n',pow.freq.semValue);
fprintf('Standard deviation: %f mHz \n',pow.freq.StD);
fprintf('Coefficient of variation: %f \n',pow.freq.CoV);
% Write footer to file
if ~writeFile('footer')
    errordlg('File not found','File Error');
end
clear pow;

end