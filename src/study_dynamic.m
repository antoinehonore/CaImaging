
function study_dynamic(info,opts)

load(fullfile(info.filepath, ['freq_' opts.userdata{1} '.mat']))

nframes=    size(freq,2);
X=          zeros(length(freq{2,1}.fullvec),nframes);
X_mean=     zeros(length(freq{2,1}.fullvec),nframes);
for j=1:nframes
    X_mean(:,j)=    freq{1,j}.fullvec';
    X(:,j)=         freq{2,j}.fullvec';
end
%%
X_mean( sum(X > 200, 2) >= 1, :)=  [];
X(      sum(X > 200, 2) >= 1, :)=  [];

% X=  X_mean;
%%
ncells=size(X,1);
control=        X(:,1);
Xp=             zeros(size(X));
Xp(:,1)=        1;

% figure, plot(ones(1,sum(idx==1)),X(idx==1,:),'b.'); hold on;
% plot(ones(1,sum(idx==2)),X(idx==2,:),'r.');
% fig_histogram=      figure;
fig_scatter1=       figure('visible','off','units','normalized','outerposition',[0 0 1 1]);
% fig_scatter2=       figure;
% pts=                -1:.001:1;
% lopts.linewidth=    3;
fig_flat=           figure('visible','off','units','normalized','outerposition',[0 0 1 1]);
fig_flat2=           figure('visible','off','units','normalized','outerposition',[0 0 1 1]);


figure(fig_flat);
plot(ones(size(X(:,1))),X(:,1),'.','DisplayName','Control'); hold on;
xticklabels=cell(1,nframes);
xticklabels{1}='Control';

for j = 2:nframes
    %     Xp(:,j)=    (X(:,j) - control) ./ control;
    %     [f,x]=      ksdensity(Xp(:,j),pts);
    %     plot(x,f,'DisplayName', ['Period ' num2str(j) ' post-control'],lopts); hold on;
    
    Xp(:,j)=    100*(X(:,j) - control) ./ control;
    
    %     figure(fig_histogram),
    %     histogram(Xp(:,j),25); hold on;
    
    figure(fig_scatter1);
    plot(control, Xp(:,j), '.', 'MarkerSize', 20); hold on;
    
    figure(fig_flat);
    plot(j*ones(size(X(:,j))),X(:,j), '.', 'DisplayName', ['Period ' num2str(j-1) ' after control']); hold on;
    xticklabels{j}=sprintf('Period %d post-ct',j-1);
end
%     figure(fig_scatter1);

% figure(fig_scatter2);
% plot(control, min(Xp(:,2:end),[],2), '.', 'MarkerSize', 20);
% legend('show');
% title({'Frequency min Increase vs control value', '(Relatively to the control period)', info.filename});
% ylabel('%increase / control'); xlabel('Control frequency value');
% set(gca, 'fontsize', 25);
% grid on; grid minor
%
%
% figure(fig_histogram),
% legend('show');
% title({'Frequency Increase Histogram', '(Relatively to the control period)', strrep(info.filename,'_','\_')});
% xlabel('Percentage of increase'); ylabel('Density');
% set(gca, 'fontsize', 25);
% grid on; grid minor


% saveas( fig_flat, fullfile(info.datapath, ['flat_fig_' info.filename(1:end-4) '.pdf']),'pdf') ;

% close all


iabove=         Xp(:, opts.study_dynamic.frame) > opts.study_dynamic.th;
ibelow=         Xp(:, opts.study_dynamic.frame) <= opts.study_dynamic.th;

%%
figure(fig_flat);
c=colormap(jet);
fullcolor=zeros(length(iabove),size(c,2));
fullcolor(iabove,:)= ones(sum(iabove),1)*c(55,:);
fullcolor(ibelow,:)= ones(sum(ibelow),1)*c(15,:);
%% Color the clusters on control
	c=  colormap(jet);    
    figure(fig_flat2);
if opts.study_dynamic.clusterPeriod>0
    Xcluster=X(:,opts.study_dynamic.clusterPeriod);
%     Xcluster=X;
    %Try k means
    Ktrunk=	[2:15];
    K=  [Ktrunk, Ktrunk, Ktrunk, Ktrunk];
  
    intra_var=[];
    for ik=1:length(K)
        nk= K(ik);
        [idx(:,ik), C, nrj(ik)]= mykmeans(Xcluster', nk);
        intra_var=[];

        for ik_plot= 1 : nk
            intra_var(ik_plot,:)= mean(var( X(idx(:,ik) == ik_plot,:) ));
            %         plot([1:nframes]',X(idx==ik_plot,:)','.','color',c(round(size(c,1)*ik_plot/nk),:) ,'LineWidth',.1); hold on;
        end
        %         title( sprintf('N clusters: %d', nk) );
         
        inter_var=  1 / mean(var( C,[],2 ), 'omitnan');
        cost(ik)=   sqrt( inter_var * mean( intra_var, 'omitnan' ) );
    end
    for ik=1:length(Ktrunk)
        Cost(ik)=   mean( cost(ik : length(Ktrunk):length(cost)) , 'omitnan');
    end
    %     figure, plot(Cost);
    [~,tmp_idx]=min(Cost);
    
    final_idx=      idx(:, tmp_idx);
    nk=             Ktrunk(tmp_idx);
    
    
    for ik=1:nk
        fullcolor(final_idx==ik,:)=ones(sum(final_idx==ik),1)*c(round(size(c,1)*ik/nk),:);
    end
    
   % now plot


    for icell=1:size(X,1)

        plot3(final_idx(icell),ones(size(X(icell,1))),X(icell,1),'k.','DisplayName','Control'); hold on;
    
        for j = 2 : nframes
            plot3(final_idx(icell),j*ones(size(X(icell,j))),X(icell,j), 'k.'); hold on;
        end
        plot3(final_idx(icell)*ones(1,nframes),[1:nframes]',X(icell,:)','--','color',fullcolor(icell,:)' ,'LineWidth',.2); hold on;

end



end%If clusters asked


figure(fig_flat);
for icell=1:ncells

	plot([1:nframes]',X(icell,:)','--','color',fullcolor(icell,:)' ,'LineWidth',.2); hold on;
end  


% saveas( figure(fig_flat), fullfile(info.datapath, ['lines_fig_' info.filename(1:end-4) '.pdf']),'pdf')

%% Figure property
figure(fig_scatter1);
legend(xticklabels(2:end));
title({'Frequency Increase vs control value', '(Relatively to the control period)', strrep(info.filename,'_','\_') });
ylabel('%increase / control'); xlabel('Control frequency value');
set(gca, 'fontsize', 25);
grid on; grid minor
h=gcf;
set(h,'PaperOrientation','landscape');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);

% saveas( fig_scatter1, fullfile(info.datapath, ['percentIncrease_VS_controlFrequencyvalue_fig_' info.filename(1:end-4)  '.pdf']),'pdf');

figure(fig_flat);
title({strrep(info.filename,'_','_'),'Flat plot, one column is one analysis period',sprintf('Parameters: %s', struct2str(opts.study_dynamic) )});
xlabel('Period'); ylabel('Frequency value (mHz)');
set(gca,    'Xtick',  [1:nframes],...
    'XTickLabel', xticklabels  );
set(gca, 'fontsize', 25);
grid on; grid minor; xlim([.5 nframes+.5])
h=gcf;
set(h,'PaperOrientation','landscape');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);

figure(fig_flat2);
title({strrep(info.filename,'_','_'),'3D plot, See the clusters individually',sprintf('Parameters: %s', struct2str(opts.study_dynamic) )});
xlabel('Clusters'); ylabel('Period');zlabel('Frequency value (mHz)');
set(gca,    'Ytick',  [1:nframes],...
    'YTickLabel', xticklabels  );
set(gca, 'fontsize', 25);
grid on; grid minor; ylim([.5 nframes+.5]);
h=gcf;
set(h,'PaperOrientation','landscape');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);

%% write to file
outfile=                    'study_dynamic.txt';
fid=            fopen(fullfile(info.datapath,outfile),'a');
fprintf(fid, 'Date: %s, parameters: %s', datestr(now), struct2str(opts.study_dynamic) ); fprintf(fid,'\n');

allOneString=   sprintf('%.0f,' , find(iabove)');
allOneString=   allOneString(1:end-1);% strip final comma
fprintf(fid, '>th %d/%d %s\nperiod: control frame1 frame2 frame3\n',sum(iabove),length(iabove), allOneString);
fprintf(fid, 'mean %f %f %f %f\n', mean(  X(  iabove  ,: ) ) );
fprintf(fid, 'std %f %f %f %f\n',  std(  X(  iabove  ,: ) ) );

allOneString=   sprintf('%.0f,' , find(ibelow)');
allOneString=	allOneString(1:end-1);% strip final comma
fprintf(fid, '<=th %d/%d %s\nperiod: control frame1 frame2 frame3\n',sum(ibelow),length(ibelow), allOneString);
fprintf(fid, 'mean %f %f %f %f\n', mean(  X(  ibelow  ,: ) ) );
fprintf(fid, 'std %f %f %f %f\n',  std(  X(  ibelow  ,: ) ) );
fclose(fid);


figure(fig_scatter1);
h=gcf;
print(h, '-dpdf', fullfile(info.filepath, ['percentIncrease_VS_controlFrequencyvalue_fig_' opts.userdata{1} '_' struct2str(opts.study_dynamic) '_' info.filename(1:end-4)  '.pdf']));

figure(fig_flat);
h=gcf;
print(h, '-dpdf', fullfile(info.filepath, ['flat_fig_' opts.userdata{1} '_' struct2str(opts.study_dynamic) '_' info.filename(1:end-4) '.pdf']));

end