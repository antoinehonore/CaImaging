function [freq,filedata]=runSpecA(DATA,info,opts)
%function [freq,filedata]=runSpecA(DATA,info,opts)
% Iterate on the possible data (time frame and (mean or deltaf)

[nl,ncol]=size(DATA);
filedata=[];

freq=cell(nl,ncol);

for i=1:nl
    for j=1:ncol
        fprintf('\nSignal: %s\n',info.filen{i,j});
        
        freq{i,j}=   performSpecA(DATA{i,j},info.filen{i,j},info, opts.specA);
        
        append={        info.filen{i,j};...
                        freq{i,j}.meanValue;...
                        freq{i,j}.semValue;...
                        freq{i,j}.StD;...
                        freq{i,j}.CoV};
                    
        filedata=[filedata, append];
    end
end
save(fullfile(info.filepath,['freq_' opts.userdata{1} '.mat']),'freq');
end