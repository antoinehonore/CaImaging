function [DATA,info]= initSpecA(info,opts)
%function [DATA,info]= initSpecA(info,opts)
% Build a structure DATA easy to operate on, and add the information
% regarding the batch number to the info structure

if isfield(opts,'netidout')
    netidout=opts.netidout;
else
    allfiles=dir(fullfile(info.filepath,['*netid_' opts.userdata{1} '*']));
    load(fullfile(info.filepath,allfiles(1).name))
end
nframe=length(netidout);

DATA=cell(2, nframe);
for iframe=1:nframe
    DATA{1,iframe}=netidout{iframe}.mean;
    DATA{2,iframe}=netidout{iframe}.deltaf;
    info.filen{1,iframe}=netidout{iframe}.filen.mean;
    info.filen{2,iframe}=netidout{iframe}.filen.deltaf;
end
end

