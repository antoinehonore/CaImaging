%Automatized version of Spectral Analysis and network identification of Calcium Luminosity Oscillations proposed
%by Per Uhlen and Erik Smedler
% 2017-04-10, Karolinska Institutet, Stockholm, Sweden: Antoine Honore
% The code is commented. Readme.txt for more general information (not done yet).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all
clc
addpath(genpath('matlab_bgl'))
%Choose the datafile root folder
info.datapath =     uigetdir(fullfile(pwd, '..','data'), 'Choose root folder of data files');

%Name the result file which will be created in the root folder
info.outfile=       'SpecA';

% Find all the data files in all subdirectories of the root folder
allFiles=           subdir( fullfile(info.datapath, '*_data.dat') );

%Display dialog boxes with the choice of options, the same options will be
%used for every data file in the subfolders of the previously chosen root folder
opts=               getopts();

% Iterate on every data files
for ifile= 1 : length( allFiles )
    try
    % Get the information of a particular data file
    info= getinfo( allFiles(ifile), info );
    
    %% NETWORK IDENTIFICATION
    %     Proceed
    if opts.actions.netid
        [opts.netidout, fn]=  networkidentification(info, opts);
        fprintf('\nNetwork Identification is done.\n\n');
    end
    %% Quantification
        if opts.actions.netQ
         netquant(info,opts)
         fprintf('\nNetwork Quantification is done.\n\n');
      end
    %% SPECTRAL ANALYSIS
    if opts.actions.specA
        % Load the data in a handy variable DATA and update info with
        % specification (mean or deltaF signal)
        [DATA, info]=       initSpecA(info, opts);
        % Proceed
        [freq, filedata]=   runSpecA(DATA, info, opts);
        fprintf('\nSpectral Analysis is done.\n\n');
        saveSpecA(freq, filedata, info);
    end
    %% In case figures have been open
    close all
    
    %% Study frequency variation
    if opts.actions.studyD
        study_dynamic(info, opts);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Plot networks
    if opts.actions.plot_net
       plot_net(info);
    end
end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
%% Open result file
openresults(info)

