clear all
close all
clc
opts.restriction=10^-4;

vopts.LineWidth=3;
coormat=[];

[pathname]=uigetdir('Choose file to plot','../data');
cutoff=zeros(1,0);
allfiles=dir( fullfile(pathname,'*_iddata_300_*scrambled.mat') );

for ifile=1:length(allfiles)
    
    s_fullfilename= fullfile(allfiles(ifile).folder, allfiles(ifile).name);
    %         strrep(fullfilename,'.mat','_scrambled.mat');
    fprintf(1, 'Now reading %s\n', s_fullfilename);
    s_corrdata= load( s_fullfilename );
    cutoff(ifile) = percentile(   s_corrdata.correlationmatrix( not( s_corrdata.correlationmatrix == 1) )   ,95);
    
end

opts.avg_cutoff=mean(cutoff);

fprintf('Average cutoff: %f\n',opts.avg_cutoff);

for ifile=1:length(allfiles)
    
    fullfilename= fullfile(allfiles(ifile).folder, strrep(allfiles(ifile).name,'_scrambled',''));
    load(fullfilename);
    
    
    network_fig{ifile}=  figure('Visible', 'Off','Name',strrep(allfiles(ifile).name,'_scrambled',''));
    
    plotnetworkcells(correlationmatrix,distancematrix,[xcoordinates; ycoordinates],[xcoordinates; ycoordinates],opts)
    
    figname{ifile}=strrep(fullfilename,'.mat','_network.pdf');
    
    saveas( network_fig{ifile}, figname{ifile},'pdf') ;
    
    coorvec=correlationmatrix(:);
    distvec=distancematrix(:);
    
    coormat(:,ifile)= coorvec( not(coorvec==1) & distvec < opts.restriction);
    
    
    
    
end

mu=mean( coormat );
sigma=std( coormat);

for ifile=1:length(allfiles)
    figure(network_fig{ifile});
    title({'Mean of the correlation values: ' num2str(mu(ifile))});
    set(gca,'fontsize',23);
end
%%


%
%
% plotnetworkcells(correlationmatrix,distancematrix,[xcoordinates; ycoordinates],[xcoordinates; ycoordinates])
%
% cuttoff=.6;
% %%
% load('/Volumes/Data/amaia/CaImaging/data/170403/2/170403_02-1_iddata_PGE2_601_1201.mat')
% %%
% figure,
% %%
% histogram(correlationmatrix(:));  hold on;
%
% ylabel('#');
%
%
% %%
% load('/Volumes/Data/amaia/CaImaging/data/170403/2/170403_02-1_iddata_PGE2_1_601.mat')
%
% % figure,
% histogram(correlationmatrix(:)); xlabel('Correlation Coefficients (Control)'); ylabel('#');
%
